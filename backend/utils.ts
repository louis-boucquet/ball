export function postCallback(
	res: any,
	body: any,
	err: NodeJS.ErrnoException | null
): void {
	if (!err) res.status(201).json(body);
	else {
		errorMsg(res, 500, err.message);
	}
}

export function errorMsg(res: any, status: number, msg: string) {
	console.log(msg);
	res.status(status).json({ msg });
}
