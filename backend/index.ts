import init from "./initDb";

//create db files
init();

import express, { Application } from "express";
import dotenv from "dotenv";
import cors from "cors";
import homeRouter from "./routes/home";
import playersRouter from "./routes/players";
import gameRouter from "./routes/games";

dotenv.config();
export const app: Application = express();
const port = process.env.PORT || 5000;

// Body parsing Middleware
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

app.get("/", homeRouter);
app.use("/players", playersRouter);
app.use("/games", gameRouter);

try {
	app.listen(port, (): void => {
		console.log(`Connected successfully on port ${port}`);
	});
} catch (error: any) {
	console.error(`Error occured: ${error.message}`);
}
