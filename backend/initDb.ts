import fs from "fs";

export default function init() {
	createDirIfNotExists("./db");
	createDirIfNotExists("./db/games");
	createFileIfNotExists("./db/games/games.json", []);
	createFileIfNotExists("./db/players.json", []);
}

function createDirIfNotExists(dir: string) {
	if (!fs.existsSync(dir)) {
		fs.mkdirSync(dir);
	}
}

function createFileIfNotExists(file: string, data: any) {
	if (!fs.existsSync(file)) {
		fs.writeFileSync(file, JSON.stringify(data));
	}
}
