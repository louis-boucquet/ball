import express from "express";
import fs from "fs";
//ignore because it can be unexisting on compiletime
// @ts-ignore
import data from "../db/players.json";
import { errorMsg, postCallback } from "../utils";

const playersRouter = express.Router();

let players: string[] = data;

//GET
playersRouter.get("/", (req, res) => {
	res.status(200).json(players);
});

/**
 * POST
 * body: {"name": "PlayerName"}
 */
playersRouter.post("/", (req, res) => {
	console.log("POST body to /players", req.body);

	if (!req.body || !req.body.name) errorMsg(res, 400, "Malformed request");
	else {
		//do not add already existing player names
		if (players.find((player: string) => player === req.body.name))
			errorMsg(res, 409, `Player '${req.body.name}' already exists`);
		else {
			players = [...players, req.body.name];

			fs.writeFile("./db/players.json", JSON.stringify(players), (err) => {
				postCallback(res, players, err);
			});
		}
	}
});

export default playersRouter;
