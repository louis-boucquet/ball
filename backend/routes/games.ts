import express from "express";
import fs from "fs";
import { v1 } from "uuid";
//ignore because it can be unexisting on compiletime
// @ts-ignore
import data from "../db/games/games.json";
import { postCallback } from "../utils";

const gameRouter = express.Router();
const games: string[] = data;

//GET
gameRouter.get("/", (req, res) => {
	console.log("GET to /games", req.body);
	res.status(200).json(games);
});

//POST
gameRouter.post("/", (req, res) => {
	console.log("POST to /games. Body: ", req.body);
	const gameId = v1();
	//save game
	fs.writeFile(
		`./db/games/${gameId}.json`,
		JSON.stringify(req.body.game),
		(err) => {
			if (err) postCallback(res, {}, err);
			else {
				//save id in games.json
				games.push(gameId);
				fs.writeFile(
					"./db/games/games.json",
					JSON.stringify(games),
					(errInd) => {
						postCallback(res, { gameId }, errInd);
					}
				);
			}
		}
	);
});

//GET /:id
gameRouter.get("/:id", (req, res) => {
	const gameId = req.params.id;
	console.log(`GET to /games/${gameId}`);
	fs.readFile(`./db/games/${gameId}.json`, (err, data) => {
		const game = JSON.parse(data.toString());
		console.log("game info: ", game);
		if (!err) res.status(200).json(game);
	});
});

export default gameRouter;
