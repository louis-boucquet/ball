export type Ball = {
	color: string;
	value: number;
	active: boolean;
};

export type Player = {
	name: string;
	score: number;
	active: boolean;
};

export enum Size {
	LARGE,
	MEDIUM,
	SMALL,
}
