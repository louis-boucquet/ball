import Game from "../Game";
import History from "../History";

//JS uses 32 bit signed integer for bitoperations
/**
 * Stream start:
 * • name of players in play order, comma seperated
 * • End of players list 0x0000
 * • target score 1 number
 * • Start game play stream 0x0000
 * • game play by 3-bit codes
 *
 * 3-bit stream codes:
 * • ball 1					001
 * • ball 2					010
 * • ball 4 yellow	011
 * • ball 4 white		100
 * • ball 6					101
 * • next player		000
 * • end round			111
 * • reserved				110
 */
export default function decompress(stream: number[]): History<Game> {
	const indexOfSplit = stream.indexOf(0x0000);
	const playersCodes = stream.slice(0, indexOfSplit);
	const targetScore = stream[indexOfSplit + 1];
	const streamCodes = stream.slice(indexOfSplit + 3);

	//name of players in play order
	const players = fromAscii(playersCodes).map((name) => ({
		name,
		score: 0,
		active: false,
	}));

	players[0].active = true;

	const initGame = Game.fromConfig(players, targetScore, false);

	//game history
	const games = streamCodes.reduce(reducer, [initGame]);
	const history = new History<Game>();
	history.history = games;

	return history;
}

function fromAscii(n: number[]): string[] {
	return String.fromCharCode(...n).split(",");
}

function reducer(
	prevValue: Game[],
	currentCode: number,
	currentIndex: number
): Game[] {
	const prevGame = prevValue[currentIndex];
	const newGame = prevGame.copy();
	//end of game
	if (currentCode === 7) return prevValue;

	//switch player
	if (currentCode === 0) newGame.nextPlayer();
	else newGame.hitBall(currentCode - 1);

	return [...prevValue, newGame];
}
