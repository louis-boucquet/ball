import Game from "../Game";
import History from "../History";
import { Ball } from "../Types";

//JS uses 32 bit signed integer for bitoperations
/**
 * Stream start:
 * • name of players in play order, comma seperated
 * • End of players list 0x0000
 * • target score 1 number
 * • Start game play stream 0x0000
 * • game play by 3-bit codes
 *
 * 3-bit stream codes:
 * • ball 1					001
 * • ball 2					010
 * • ball 4 yellow	011
 * • ball 4 white		100
 * • ball 6					101
 * • next player		000
 * • end round			111
 * • reserved				110
 */
export default function compress(game: History<Game>): number[] {
	//name of players in play order
	const setup = game.history[0];
	const playerNames = setup.players
		.map((p) => [...toAscii(p.name), 44]) // comma = ascii code 44
		.flat();

	//remove last comma
	playerNames.pop();
	// end of players list, targetScore, start of game play stream
	playerNames.push(0b000, setup.targetScore, 0b000);

	//game play stream

	console.log(game.history.reduce(reducer, []));

	return [...playerNames, ...game.history.reduce(reducer, [])];
}

function toAscii(s: string): number[] {
	return s.split("").map((c) => c.charCodeAt(0));
}

function reducer(
	prevValue: number[],
	currentGame: Game,
	currentIndex: number,
	history: Game[]
): number[] {
	//last game has no sequel
	if (currentIndex >= history.length - 1) return [...prevValue, 0b111];
	const nextGame = history[currentIndex + 1];

	//check if there is player switch
	if (currentGame.playerIndex !== nextGame.playerIndex)
		return [...prevValue, 0b000];

	return [...prevValue, findDifferentBall(currentGame.balls, nextGame.balls)];
}

function findDifferentBall(balls1: Ball[], balls2: Ball[]): number {
	return balls1.findIndex((b, i) => b.active !== balls2[i].active) + 1;
}
