import { Ball } from "./Types";

export const balls: Ball[] = [
	{
		value: 1,
		color: "green",
		active: true,
	},
	{
		value: 2,
		color: "red",
		active: true,
	},
	{
		value: 4,
		color: "yellow",
		active: true,
	},
	{
		value: 4,
		color: "white",
		active: true,
	},
	{
		value: 6,
		color: "blue",
		active: true,
	},
];
