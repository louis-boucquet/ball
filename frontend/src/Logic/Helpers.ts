import { Player } from "./Types";

export function shufflePlayers(players: Player[]): Player[] {
	return players.sort(() => 0.5 - Math.random());
}

export function toUppercaseWithSpaces(name: string): string {
	return (
		name[0].toUpperCase() +
		name
			.substr(1)
			.match(/[A-Z]?[a-z]+/g)!
			.map((s) => s.toLowerCase())
			.join(" ")
	);
}
