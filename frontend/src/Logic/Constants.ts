import { Ball } from "./Types";

export const BALLS: Ball[] = [
	{ color: "green", value: 1, active: true },
	{ color: "red", value: 2, active: true },
	{ color: "yellow", value: 4, active: true },
	{ color: "white", value: 4, active: true },
	{ color: "blue", value: 61, active: true },
];
