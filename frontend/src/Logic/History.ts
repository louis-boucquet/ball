export default class History<T> {
	public history: T[];
	private future: T[];

	constructor() {
		this.history = [];
		this.future = [];
	}

	static newHistoryWithElement<T>(element: T): History<T> {
		const his: History<T> = new History();
		his.push(element);
		return his;
	}

	copy(): History<T> {
		const copy: History<T> = new History();
		copy.history = [...this.history];
		copy.future = [...this.future];
		return copy;
	}

	push(element: T) {
		this.history.push(element);
		this.future.length = 0;
	}

	peek(): T | null {
		return this.historyLength() > 0
			? this.history[this.history.length - 1]
			: null;
	}

	historyLength() {
		return this.history.length;
	}

	canRedo(): boolean {
		return this.future.length > 0;
	}

	private undoRedo(a: T[], b: T[]) {
		if (a.length) {
			const e = a.pop()!;
			b.push(e);
			return true;
		}

		return false;
	}

	redo(): boolean {
		return this.undoRedo(this.future, this.history);
	}

	undo(): boolean {
		return this.undoRedo(this.history, this.future);
	}

	concat(element: T): History<T> {
		const newHistory: History<T> = this.copy();
		newHistory.push(element);
		return newHistory;
	}
}
