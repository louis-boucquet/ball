import { KeyboardEventSimple } from "./KeyboardEventSimple";

export interface KeyBindChecker {
	matches(event: KeyboardEventSimple): boolean;
}