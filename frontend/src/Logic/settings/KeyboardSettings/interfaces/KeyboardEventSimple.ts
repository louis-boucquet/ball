export interface KeyboardEventSimple {
	key: string;
	metaKey: boolean;
	shiftKey: boolean;
}