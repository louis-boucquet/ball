import { KeyBindData } from "../Model/KeyBindData";

export interface KeyBindUpdater {
	addKeyBind(bind: KeyBindData): void;
	removeKeyBind(bind: KeyBindData): void;
	getKeyBinds(): KeyBindData[];
}