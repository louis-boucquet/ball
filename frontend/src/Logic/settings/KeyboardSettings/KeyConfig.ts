import { KeyBind } from "./KeyBind";
import { actionNames, KeyConfigData } from "./Model/KeyConfigData";

export type KeyConfig = {
	[key in keyof KeyConfigData]: KeyBind
}

export function getKeyConfig() {
	const out: Partial<KeyConfig> = {};

	for (const key of actionNames) {
		out[key as keyof KeyConfigData] = new KeyBind(key as keyof KeyConfigData);
	}

	return out as KeyConfig;
}