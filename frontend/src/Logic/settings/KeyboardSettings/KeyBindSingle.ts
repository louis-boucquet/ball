import { KeyBindChecker } from "./interfaces/KeyBindChecker";
import { KeyboardEventSimple } from "./interfaces/KeyboardEventSimple";
import { KeyBindData } from "./Model/KeyBindData";

export class KeyBindSingle implements KeyBindChecker {
	constructor(
		private name: string,
		private meta?: boolean,
		private shift?: boolean
	) {}

	matches(event: KeyboardEventSimple): boolean {
		return (
			event.key === this.name &&
			(!this.meta || event.metaKey) &&
			(!this.shift || event.shiftKey)
		);
	}

	static fromData({ name, meta, shift }: KeyBindData): KeyBindSingle {
		return new KeyBindSingle(name, meta, shift);
	}
}