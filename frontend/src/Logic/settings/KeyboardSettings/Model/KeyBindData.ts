export interface KeyBindData {
	name: string;
	meta?: boolean;
	shift?: boolean;
}
