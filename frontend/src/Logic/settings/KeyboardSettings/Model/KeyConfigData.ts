import { KeyBindData } from "./KeyBindData";

export interface KeyConfigData {
	undo: KeyBindData[];
	redo: KeyBindData[];
	nextPlayer: KeyBindData[];
	claimPoints: KeyBindData[];
	selectBall0: KeyBindData[];
	selectBall1: KeyBindData[];
	selectBall2: KeyBindData[];
	selectBall3: KeyBindData[];
	selectBall4: KeyBindData[];
}

export const actionNames: (keyof KeyConfigData)[] = [
	"undo",
	"redo",
	"nextPlayer",
	"claimPoints",
	"selectBall0",
	"selectBall1",
	"selectBall2",
	"selectBall3",
	"selectBall4",
]
