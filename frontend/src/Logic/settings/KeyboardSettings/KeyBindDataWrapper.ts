import { KeyConfigData } from "./Model/KeyConfigData";

export interface KeyBindDataWrapper {
	keyConfigData: KeyConfigData;
}

export const keyBindDataWrapper: KeyBindDataWrapper = {
	get keyConfigData(): KeyConfigData {
		if (!localStorage.keys) this.keyConfigData = defaultKeyBinds;

		return JSON.parse(localStorage.keys);
	},
	set keyConfigData(binds: KeyConfigData) {
		localStorage.keys = JSON.stringify(binds);
	},
};

export const defaultKeyBinds: KeyConfigData = {
	undo: [
		{
			name: "z",
			meta: true,
		},
		{
			name: "ArrowLeft",
		},
	],
	redo: [
		{
			name: "y",
			meta: true,
		},
		{
			name: "z",
			meta: true,
			shift: true,
		},
		{
			name: "ArrowRight",
		},
	],
	nextPlayer: [{ name: "n" }],
	claimPoints: [{ name: "c" }],
	selectBall0: [{ name: "1" }],
	selectBall1: [{ name: "2" }],
	selectBall2: [{ name: "4" }],
	selectBall3: [{ name: "5" }],
	selectBall4: [{ name: "6" }],
};
