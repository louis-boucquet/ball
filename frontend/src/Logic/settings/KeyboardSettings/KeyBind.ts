import { KeyBindChecker } from "./interfaces/KeyBindChecker";
import { KeyBindUpdater } from "./interfaces/KeyBindUpdater";
import { KeyboardEventSimple } from "./interfaces/KeyboardEventSimple";
import { keyBindDataWrapper } from "./KeyBindDataWrapper";
import { KeyBindSingle } from "./KeyBindSingle";
import { KeyBindData } from "./Model/KeyBindData";
import { KeyConfigData } from "./Model/KeyConfigData";

export class KeyBind implements KeyBindChecker, KeyBindUpdater {
	constructor(public name: keyof KeyConfigData) {}

	matches(event: KeyboardEventSimple): boolean {
		return this.data
			.map((data) => KeyBindSingle.fromData(data))
			.some((keyBind) => keyBind.matches(event));
	}

	addKeyBind(bind: KeyBindData): void {
		const config: KeyConfigData = keyBindDataWrapper.keyConfigData;
		const existingBind = config[this.name].find((b) =>
			this.compareKeyBinds(b, bind)
		);

		if (!existingBind) {
			config[this.name].push(bind);
		}

		keyBindDataWrapper.keyConfigData = config;
	}

	removeKeyBind(toRemove: KeyBindData): void {
		const config: KeyConfigData = keyBindDataWrapper.keyConfigData;
		config[this.name] = config[this.name].filter(
			(bind) => !this.compareKeyBinds(bind, toRemove)
		);

		keyBindDataWrapper.keyConfigData = config;
	}

	getKeyBinds(): KeyBindData[] {
		return keyBindDataWrapper.keyConfigData[this.name];
	}

	private get data(): KeyBindData[] {
		return keyBindDataWrapper.keyConfigData[this.name];
	}

	private compareKeyBinds(a: KeyBindData, b: KeyBindData): boolean {
		return (
			a.name === b.name && !!a.meta === !!b.meta && !!a.shift === !!b.shift
		);
	}
}
