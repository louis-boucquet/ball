import { getKeyConfig } from "./KeyboardSettings/KeyConfig";

export class Settings {
	readonly keysConfig = getKeyConfig();
}

export const settings = new Settings();
