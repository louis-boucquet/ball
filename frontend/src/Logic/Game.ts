import { balls } from "./Data";
import { Ball, Player } from "./Types";

export default class Game {
	constructor(
		public balls: Ball[],
		public players: Player[],
		public playerIndex: number,
		public isNewFrame: boolean,
		public pointsToAdd: number,
		public targetScore: number,
		private lastPlayerIndex: number,
		public gameEnded: boolean,
		public useClaimPoints: boolean
	) {}

	get projectedScore() {
		return this.pointsToAdd + this.currentPlayer.score;
	}

	get currentPlayer() {
		return this.players[this.playerIndex];
	}

	copy() {
		return new Game(
			this.balls.map((p) => Object.assign({}, p)),
			this.players.map((p) => Object.assign({}, p)),
			this.playerIndex,
			this.isNewFrame,
			this.pointsToAdd,
			this.targetScore,
			this.lastPlayerIndex,
			this.gameEnded,
			this.useClaimPoints
		);
	}

	resetBalls() {
		for (const ball of this.balls) ball.active = true;
	}

	nextPlayer() {
		if (this.gameEnded) return;
		this.resetBalls();

		if (!this.useClaimPoints) {
			this.addScore();
		}

		this.currentPlayer.active = false;

		this.playerIndex++;
		this.playerIndex %= this.players.length;

		this.currentPlayer.active = true;

		if (this.lastPlayerIndex >= 0) {
			//each player has only 1 remaining chance to win

			this.lastPlayerIndex++;
			if (this.lastPlayerIndex >= this.players.length) {
				this.gameEnded = true;
			}
		}

		this.isNewFrame = false;
		this.pointsToAdd = 0;
	}

	claimPoints() {
		if (!this.useClaimPoints)
			throw new Error("Cannot claim points in this game mode");

		this.addScore();

		this.nextPlayer();
	}

	selectBall(index: number) {
		const canHit = this.balls[index].active;

		if (canHit) this.hitBall(index);

		return canHit;
	}

	hitBall(index: number) {
		if (this.isNewFrame) this.resetBalls();

		this.balls[index].active = false;

		const newScore =
			this.currentPlayer.score + this.pointsToAdd + this.balls[index].value;

		if (newScore <= this.targetScore)
			this.pointsToAdd += this.balls[index].value;

		this.isNewFrame = this.balls.every(({ active }) => !active);

		if (this.isNewFrame) {
			this.resetBalls();
			this.balls[index].active = false;
		}
	}

	getWinners() {
		return this.players.filter((p) => p.score === this.targetScore);
	}

	static fromConfig(
		players: Player[],
		targetScore: number,
		useClaimPoints: boolean
	) {
		return new Game(
			balls,
			players,
			0,
			false,
			0,
			targetScore,
			-1,
			false,
			useClaimPoints
		);
	}

	getCurrentMaxScore(): number {
		return Math.max(...this.players.map((p) => p.score));
	}

	private addScore(): void {
		this.currentPlayer.score += this.pointsToAdd;

		if (this.currentPlayer.score === this.targetScore) {
			this.lastPlayerIndex = this.playerIndex;
		}
	}

	equals(other: Game): boolean {
		function equalsPlayer(p1: Player, p2: Player): boolean {
			return (
				p1.name === p2.name && p1.score === p2.score && p1.active === p2.active
			);
		}

		function equalsBalls(b1: Ball, b2: Ball): boolean {
			return (
				b1.color === b2.color &&
				b1.value === b2.value &&
				b1.active === b2.active
			);
		}

		return (
			this.playerIndex === other.playerIndex &&
			this.isNewFrame === other.isNewFrame &&
			this.pointsToAdd === other.pointsToAdd &&
			this.targetScore === other.targetScore &&
			this.lastPlayerIndex === other.lastPlayerIndex &&
			this.gameEnded === other.gameEnded &&
			this.useClaimPoints === other.useClaimPoints &&
			this.players.length === other.players.length &&
			this.players
				.map((p, i) => [p, other.players[i]])
				.every(([p1, p2]) => equalsPlayer(p1, p2)) &&
			this.players.length === other.players.length &&
			this.balls
				.map((b, i) => [b, other.balls[i]])
				.every(([b1, b2]) => equalsBalls(b1, b2))
		);
	}
}
