export const backend = process.env.REACT_APP_BACKEND;

/**
 * General http headers for each request.
 * */
export function getHeaders() {
	return {
		"Content-type": "application/json",
		Accept: "application/json",
	};
}
