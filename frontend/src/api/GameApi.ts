import { backend, getHeaders } from "./Api";

export function getGames() {
	return fetch(`${backend}/games/`).then((res) => {
		return res.json();
	});
}

export function saveGame(game: number[]): Promise<string[]> {
	return fetch(`${backend}/games/`, {
		method: "POST",
		headers: getHeaders(),
		body: JSON.stringify({ game }),
	}).then((res) => {
		return res.json();
	});
}
