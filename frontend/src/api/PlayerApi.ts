import { backend, getHeaders } from "./Api";

export function getPlayers() {
	return fetch(`${backend}/players/`).then((res) => {
		return res.json();
	});
}

export function addPlayer(name: string): Promise<string[]> {
	return fetch(`${backend}/players/`, {
		method: "POST",
		headers: getHeaders(),
		body: JSON.stringify({ name }),
	}).then((res) => {
		return res.json();
	});
}
