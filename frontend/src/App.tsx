import React, { useState } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { Player } from "./Logic/Types";
import Home from "./Components/home/Home";
import GameComponent from "./Components/GameComponent";
import Game from "./Logic/Game";
import Settings from "./Components/Settings/Settings";
import Header from "./Components/header/Header";

export default function App() {
	const [players, setPlayers] = useState<Player[]>([]);
	const [targetScore, setTargetScore] = useState<string>("100");
	const [useClaimPoints, setUseClaimPoints] = useState<boolean>(false);

	return (
		<Router>
			<Header />
			<Switch>
				<Route path="/game">
					<GameComponent
						startGame={Game.fromConfig(
							players,
							parseInt(targetScore),
							useClaimPoints
						)}
					/>
				</Route>
				<Route path="/settings">
					<Settings />
				</Route>
				<Route path="/">
					<Home
						players={players}
						setPlayers={setPlayers}
						setTargetScore={setTargetScore}
						targetScore={targetScore}
						setUseClaimPoints={setUseClaimPoints}
						useClaimPoints={useClaimPoints}
					/>
				</Route>
			</Switch>
		</Router>
	);
}
