import React, { useState } from "react";
import { Player } from "../../Logic/Types";
import AddPlayerForm from "./AddPlayerForm";
import GameSettingsForm from "./GameSettingsForm";
import SelectedPlayersList from "./SelectedPlayersList";
import { useEffectOnce } from "react-use";
import { getPlayers } from "../../api/PlayerApi";
import { Redirect } from "react-router-dom";
import KnownPlayersList from "./KnownPlayersList";

export default function Home({
	players,
	setPlayers,
	targetScore,
	setTargetScore,
	useClaimPoints,
	setUseClaimPoints,
}: {
	players: Player[];
	setPlayers: (players: Player[]) => void;
	targetScore: string;
	setTargetScore: (targetScore: string) => void;
	useClaimPoints: boolean;
	setUseClaimPoints: (useClaimPoints: boolean) => void;
}) {
	const [existingPlayers, setExistingPlayers] = useState<string[]>([]);
	const [startGame, setStartGame] = useState<boolean>(false);

	useEffectOnce(() => {
		getPlayers().then(setExistingPlayers);
	});

	return startGame ? (
		<Redirect to="/game" />
	) : (
		<div className="flex h-body justify-center flex-col">
			<div className="flex w-full h-3/5 px-60 py-20 justify-around space-x-16">
				<div className="flex flex-col space-y-5 h-full w-3/5 justify-center">
					<AddPlayerForm
						players={players}
						setPlayers={setPlayers}
						setExistingPlayers={setExistingPlayers}
					/>
					<GameSettingsForm
						players={players}
						setPlayers={setPlayers}
						targetScore={targetScore}
						setTargetScore={setTargetScore}
						useClaimPoints={useClaimPoints}
						setUseClaimPoints={setUseClaimPoints}
						setStartGame={setStartGame}
					/>
				</div>
				<div className="flex flex-col h-full w-2/5">
					<SelectedPlayersList players={players} setPlayers={setPlayers} />
					<KnownPlayersList
						existingPlayers={existingPlayers}
						players={players}
						setPlayers={setPlayers}
					/>
				</div>
			</div>
		</div>
	);
}
