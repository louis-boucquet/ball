import React, { useState } from "react";
import { NavLink } from "react-router-dom";
import { shufflePlayers } from "../../Logic/Helpers";
import { Player } from "../../Logic/Types";

export default function GameSettingsForm({
	players,
	setPlayers,
	targetScore,
	setTargetScore,
	useClaimPoints,
	setUseClaimPoints,
	setStartGame,
}: {
	players: Player[];
	setPlayers: (players: Player[]) => void;
	targetScore: string;
	setTargetScore: (targetScore: string) => void;
	useClaimPoints: boolean;
	setUseClaimPoints: (useClaimPoints: boolean) => void;
	setStartGame: (startGame: boolean) => void;
}) {
	const [isShufflePlayers, setIsShufflePlayers] = useState<boolean>(true);

	const canStartGame = players.length && targetScore;

	return (
		<form
			className="flex flex-col space-y-5"
			onSubmit={(e) => {
				e.preventDefault();
				if (canStartGame) setStartGame(true);
			}}
		>
			<input
				className="mr-0 w-3/5"
				value={targetScore}
				type="number"
				min={1}
				required={true}
				onChange={(e) => {
					setTargetScore(e.target.value);
				}}
				placeholder="Score"
			/>
			<div className="flex items-center text-2xl">
				<label htmlFor="shufflePlayers">Shuffle players</label>
				<input
					className="cursor-pointer w-5 h-5 ml-6"
					name="shufflePlayers"
					type="checkbox"
					onChange={() => setIsShufflePlayers(!isShufflePlayers)}
					checked={isShufflePlayers}
				/>
			</div>
			<div className="flex items-center text-2xl">
				<label htmlFor="useClaimPoints">Use claim points</label>
				<input
					className="cursor-pointer w-5 h-5 ml-2"
					name="useClaimPoints"
					type="checkbox"
					onChange={() => setUseClaimPoints(!useClaimPoints)}
					checked={useClaimPoints}
				/>
			</div>
			<NavLink
				className={`${canStartGame ? "" : "disabled"} w-3/5`}
				onClick={(e) => {
					if (!canStartGame) e.preventDefault();
					if (isShufflePlayers) setPlayers(shufflePlayers(players));
				}}
				to="/game"
			>
				Start game
			</NavLink>
		</form>
	);
}
