import React, { useState } from "react";
import { addPlayer } from "../../api/PlayerApi";
import { Player } from "../../Logic/Types";

export default function AddPlayerForm({
	players,
	setPlayers,
	setExistingPlayers,
}: {
	players: Player[];
	setPlayers: (players: Player[]) => void;
	setExistingPlayers: (players: string[]) => void;
}) {
	const [playerName, setPlayerName] = useState<string>("");

	return (
		<form
			onSubmit={(e) => {
				e.preventDefault();
				if (playerName) {
					addPlayer(playerName).then(setExistingPlayers);
					setPlayers([
						...players,
						{
							name: playerName,
							score: 0,
							active: players.length === 0,
						},
					]);
					setPlayerName("");
				}
			}}
		>
			<input
				className="mr-5 w-3/5"
				value={playerName}
				type="text"
				onChange={(e) => setPlayerName(e.target.value)}
				placeholder="New player"
				autoFocus={true}
			/>
			<button>Add player</button>
		</form>
	);
}
