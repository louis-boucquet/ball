import React from "react";
import { FaTrash } from "react-icons/all";
import { Player } from "../../Logic/Types";

export default function SelectedPlayersList({
	players,
	setPlayers,
}: {
	players: Player[];
	setPlayers: (players: Player[]) => void;
}) {
	return (
		<div className="flex flex-col w-full h-1/2">
			<h2>Players</h2>
			{players.length ? (
				<ul className="overflow-scroll">
					{players.map((p, i) => (
						<li
							key={i}
							className="flex justify-between items-center cursor-pointer hover:line-through hover:text-red"
							onClick={() =>
								setPlayers(players.slice(0, i).concat(players.slice(i + 1)))
							}
						>
							<span>{p.name}</span>
							<FaTrash className="text-base" />
						</li>
					))}
				</ul>
			) : (
				<span>Currently no players added</span>
			)}
		</div>
	);
}
