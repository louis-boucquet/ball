import React from "react";
import { Player } from "../../Logic/Types";

export default function KnownPlayersList({
	existingPlayers,
	players,
	setPlayers,
}: {
	existingPlayers: string[];
	players: Player[];
	setPlayers: (players: Player[]) => void;
}) {
	return (
		<div className="w-full h-1/2">
			<h2>Select from known players</h2>
			<ul className="text-xl overflow-scroll">
				{existingPlayers
					.filter((ep) => !players.find((p) => ep === p.name))
					.map((p, i) => (
						<li
							key={i}
							className="flex justify-between items-center cursor-pointer hover:text-grey-2"
							onClick={() =>
								setPlayers([
									...players,
									{ name: p, score: 0, active: players.length === 0 },
								])
							}
						>
							{p}
						</li>
					))}
			</ul>
		</div>
	);
}
