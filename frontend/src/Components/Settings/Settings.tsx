import React, { useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import { settings } from "../../Logic/settings/Settings";
import { FaTrash } from "react-icons/all";
import { KeyBindData } from "../../Logic/settings/KeyboardSettings/Model/KeyBindData";
import { KeyBind } from "../../Logic/settings/KeyboardSettings/KeyBind";
import { useEffectOnce } from "react-use";
import BallComponent from "../Balls/BallComponent";
import { toUppercaseWithSpaces } from "../../Logic/Helpers";
import { Size } from "../../Logic/Types";

const defaultKeyToAdd = { name: "" };

export default function Settings() {
	function keyBindDataToString(kbd: KeyBindData): string {
		return `${kbd.meta ? "Cmd+" : ""}${kbd.shift ? "Shift+" : ""}${
			kbd.name.length > 1 ? kbd.name : kbd.name.toUpperCase()
		}`;
	}

	const [currentKeyBind, setCurrentKeyBind] = useState<{
		html: JSX.Element;
		kb: KeyBind;
	}>({ html: <div>Undo</div>, kb: settings.keysConfig.undo });
	const [rerender, setRerender] = useState<boolean>(false);
	const [keyToAdd, setKeyToAdd] = useState<KeyBindData>(defaultKeyToAdd);
	const [selectedIndex, setSelectedIndex] = useState<number>(0);

	useEffect(() => {
		setKeyToAdd(defaultKeyToAdd);
	}, [currentKeyBind]);

	useEffectOnce(() => {
		const listener: (this: Document, ev: DocumentEventMap["keydown"]) => any = (
			e
		) => {
			setKeyToAdd({
				name: e.key,
				meta: e.metaKey,
				shift: e.shiftKey,
			});
		};

		document.addEventListener("keydown", listener);

		return () => {
			document.removeEventListener("keypress", listener);
		};
	});

	const listElements = [
		...[
			settings.keysConfig.undo,
			settings.keysConfig.redo,
			settings.keysConfig.claimPoints,
			settings.keysConfig.nextPlayer,
		].map((kb) => ({
			html: <div className="text-2xl">{toUppercaseWithSpaces(kb.name)}</div>,
			kb,
		})),
		...[
			{ keyBind: settings.keysConfig.selectBall0, value: 1, color: "green" },
			{ keyBind: settings.keysConfig.selectBall1, value: 2, color: "red" },
			{ keyBind: settings.keysConfig.selectBall2, value: 4, color: "yellow" },
			{ keyBind: settings.keysConfig.selectBall3, value: 4, color: "white" },
			{ keyBind: settings.keysConfig.selectBall4, value: 6, color: "blue" },
		].map(({ keyBind, value, color }) => {
			return {
				html: (
					<div className="flex items-center text-2xl">
						<div className="mr-4">Select </div>
						<BallComponent
							ball={{
								color,
								value,
								active: true,
							}}
							size={Size.MEDIUM}
						/>
					</div>
				),
				kb: keyBind,
			};
		}),
	];

	return (
		<div className="flex flex-col w-full h-body p-20 justify-center items-center">
			<div className="flex w-4/5 justify-center items-center h-full">
				<ul className="border-r-4 p-5 w-3/5 h-full">
					{listElements.map(
						({ html, kb }: { html: JSX.Element; kb: KeyBind }, i: number) => (
							<li
								key={i}
								className={`p-2 cursor-pointer hover:text-blue ${
									i === selectedIndex
										? "bg-grey-2 rounded-xl bg-opacity-20"
										: ""
								}`}
								onClick={() => {
									setSelectedIndex(i);
									setCurrentKeyBind({ html, kb });
								}}
							>
								{html}
							</li>
						)
					)}
				</ul>
				<div className="flex flex-col justify-between p-5 w-2/5 h-full">
					<div className="flex flex-col mb-5">
						<h1 className="flex justify-center border-b-4 pb-2">
							{currentKeyBind.html}
						</h1>
						<ul className="flex flex-col text-2xl pt-4">
							{currentKeyBind.kb.getKeyBinds().map((kb, i) => (
								<li key={i} className="flex items-center justify-between">
									<div className="mr-2">{keyBindDataToString(kb)}</div>
									<FaTrash
										className="cursor-pointer text-2xl hover:text-red"
										onClick={() => {
											currentKeyBind.kb.removeKeyBind(kb);
											setRerender(!rerender); //Re-render
										}}
									/>
								</li>
							))}
						</ul>
					</div>
					<div className="flex flex-col justify-between items-center">
						<div
							className={`bg-main-2 w-full text-xl m-5 p-2 flex justify-center items-center cursor-default rounded-lg 
						${keyToAdd.name === "" ? "text-grey-2" : ""}`}
						>
							{keyToAdd.name === ""
								? "Press a Key"
								: keyBindDataToString(keyToAdd)}
						</div>
						<button
							className="disabled:cursor-not-allowed disabled:opacity-50"
							onClick={() => {
								currentKeyBind.kb.addKeyBind(keyToAdd);
								setKeyToAdd(defaultKeyToAdd);
								setRerender(!rerender); //Re-render
							}}
							disabled={keyToAdd.name === ""}
						>
							Add
						</button>
					</div>
				</div>
			</div>
			<NavLink to="/">Home</NavLink>
		</div>
	);
}
