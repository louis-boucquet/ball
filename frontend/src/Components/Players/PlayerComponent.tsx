import React from "react";
import Game from "../../Logic/Game";
import { Player } from "../../Logic/Types";
import { FaCrown } from "react-icons/fa";

export default function PlayerComponent({
	game,
	player,
	isFirst,
}: {
	game: Game;
	player: Player;
	isFirst: boolean;
}) {
	return (
		<div className={`p-8 rounded-lg ${player.active ? "bg-main-4" : ""}`}>
			<div className="flex">
				<h1>{player.name}</h1>
				{isFirst ? <FaCrown className="text-2xl text-yellow ml-2" /> : null}
			</div>
			<div className="mb-4 text-2xl">Score: {player.score}</div>
			{player.active && (
				<>
					<div className="text-xl text-grey-1">
						Points made: {game.pointsToAdd}
					</div>
					<div className="text-xl text-grey-1">
						Projected score: {game.projectedScore}
					</div>
				</>
			)}
		</div>
	);
}
