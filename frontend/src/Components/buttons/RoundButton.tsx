import React from "react";

export default function RoundButton({ text }: { text: string }) {
	return <button className="bg-main-1 p-4 m-2 text-2xl">{text}</button>;
}
