import React from "react";

export default function UndoRedoButton({
	flipH,
	text,
	onClick,
	disabled,
	tooltip,
}: {
	flipH: boolean;
	text: string;
	onClick: () => void;
	disabled: boolean;
	tooltip: string;
}) {
	return (
		<div
			className={`flex flex-col items-center rounded-xl bg-main-4 cursor-pointer p-4 shadow ${
				disabled ? "disabled" : "border-active"
			}`}
			onClick={onClick}
			title={`Shortcut: [${tooltip}]`}
		>
			<svg height="40px" version="1.1" viewBox="0 0 512 512" width="40px">
				<path
					d="M447.9,368.2c0-16.8,3.6-83.1-48.7-135.7c-35.2-35.4-80.3-53.4-143.3-56.2V96L64,224l192,128v-79.8   c40,1.1,62.4,9.1,86.7,20c30.9,13.8,55.3,44,75.8,76.6l19.2,31.2H448C448,389.9,447.9,377.1,447.9,368.2z"
					transform={flipH ? "scale(-1,1) translate(-512,0)" : ""}
				/>
			</svg>
			<span className="text-2xl font-bold">{text}</span>
		</div>
	);
}
