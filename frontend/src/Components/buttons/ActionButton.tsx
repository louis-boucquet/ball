import React from "react";

export default function ActionButton({
	text,
	onClick,
	tooltip,
}: {
	text: string;
	onClick: () => void;
	tooltip: string;
}) {
	return (
		<button
			className={`bg-main-4 p-4 text-2xl relative
			hover:bg-main-1 shadow rounded-xl`}
			onClick={() => {
				onClick();
			}}
			title={tooltip}
		>
			{text}
		</button>
	);
}
