import ReactModal from "react-modal";
import React from "react";
import { FaTimes } from "react-icons/fa";
import { Player } from "../Logic/Types";

export function EndScreenPopUp({
	isOpen,
	onRequestClose,
	title,
	winners,
}: {
	isOpen: boolean;
	onRequestClose: () => void;
	title: string;
	winners: Player[];
}) {
	const lastPlayer = winners.pop();
	return (
		<ReactModal
			ariaHideApp={false}
			isOpen={isOpen}
			appElement={window.document.getElementById("root") as HTMLElement}
			onRequestClose={onRequestClose}
			className="dialog-body bg-main-4 absolute left-1/2 top-1/2 overflow-auto outline-none rounded-sm shadow-md transform -translate-x-1/2 -translate-y-1/2"
			overlayClassName="z-20 fixed bg-black bg-opacity-60 top-0 right-0 left-0 bottom-0"
			bodyOpenClassName="overflow-hidden"
			shouldCloseOnOverlayClick={true}
			shouldCloseOnEsc={true}
		>
			<div className="flex z-10 bg-main-4 sticky top-0 left-0 bottom-0 right-0 shadow-sm border-b p-8">
				<p className="font-bold text-4xl mr-2">
					Game was won by {winners.map((p) => p.name).join(", ")}
					{winners.length ? ` and ` : null}
					{lastPlayer?.name}
				</p>
				<div
					className="flex ml-auto items-center justify-center cursor-pointer"
					onClick={() => {
						onRequestClose();
					}}
				>
					<FaTimes className="text-2xl text-main-2" />
				</div>
			</div>
		</ReactModal>
	);
}
