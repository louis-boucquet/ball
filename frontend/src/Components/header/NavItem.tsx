import React, { useState } from "react";
import { NavLink } from "react-router-dom";
import BallComponent from "../Balls/BallComponent";
import { Ball, Size } from "../../Logic/Types";

export default function NavItem({
	to,
	text,
	ball,
}: {
	to: string;
	text: string;
	ball: Ball;
}) {
	const [showBall, setShowBall] = useState<boolean>(false);
	return (
		<NavLink
			to={to}
			className="flex justify-center items-center bg-transparent p-0 border-b-2 border-transparent rounded-none hover:border-grey-2 hover:text-grey-1"
			onMouseEnter={() => setShowBall(true)}
			onMouseLeave={() => setShowBall(false)}
		>
			<div className={`${showBall ? "visible" : "invisible"} mr-1`}>
				<BallComponent ball={ball} size={Size.SMALL} />
			</div>
			<span>{text}</span>
		</NavLink>
	);
}
