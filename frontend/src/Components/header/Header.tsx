import React from "react";
import NavItem from "./NavItem";
import { BALLS } from "../../Logic/Constants";

export default function Header() {
	return (
		<div className="flex justify-center items-center w-full bg-gradient-to-b from-grey-2 h-header">
			<div className="flex justify-around h-full w-1/2">
				<NavItem to="/" text="Ball" ball={BALLS[0]} />
				<NavItem to="/settings" text="Settings" ball={BALLS[1]} />
			</div>
		</div>
	);
}
