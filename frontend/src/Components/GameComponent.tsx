import React, { useState } from "react";
import { Redirect } from "react-router-dom";
import BallsComponent from "./Balls/BallsComponent";
import PlayerComponent from "./Players/PlayerComponent";
import Game from "../Logic/Game";
import RoundButton from "./buttons/RoundButton";
import History from "../Logic/History";
import ActionButton from "./buttons/ActionButton";
import UndoRedoButton from "./buttons/UndoRedoButton";
import { EndScreenPopUp } from "./EndScreenPopUp";
import { settings } from "../Logic/settings/Settings";
import compress from "../Logic/converters/Compressor";
import decompress from "../Logic/converters/Decompressor";
import { saveGame } from "../api/GameApi";

export default function GameComponent({ startGame }: { startGame: Game }) {
	const [game, setGame] = useState<Game>(startGame);
	const [history, setHistory] = useState<History<Game>>(
		History.newHistoryWithElement(startGame)
	);
	const [hasClosed, setHasClosed] = useState<boolean>(false);

	function nextPlayer() {
		const newGame = game.copy();

		newGame.nextPlayer();

		setGame(newGame);
		setHistory(history.concat(newGame));
	}

	function claimPoints() {
		const newGame = game.copy();

		newGame.claimPoints();

		setGame(newGame);
		setHistory(history.concat(newGame));
	}

	function selectBall(index: number) {
		const newGame = game.copy();

		const success = newGame.selectBall(index);

		if (success) {
			setGame(newGame);
			setHistory(history.concat(newGame));
		}
	}

	function undo() {
		if (history.historyLength() > 1 && history.undo()) {
			setGame(history.peek()!);
			setHistory(history);
			setHasClosed(false);
		}
	}

	function redo() {
		if (history.redo()) {
			setGame(history.peek()!);
			setHistory(history.copy());
		}
	}

	function handleKeyPress(event: React.KeyboardEvent<HTMLDivElement>) {
		if (settings.keysConfig.redo.matches(event)) redo();
		else if (settings.keysConfig.undo.matches(event)) undo();
		else if (settings.keysConfig.nextPlayer.matches(event)) nextPlayer();
		else if (
			settings.keysConfig.claimPoints.matches(event) &&
			game.useClaimPoints
		)
			claimPoints();
		else if (settings.keysConfig.selectBall0.matches(event)) selectBall(0);
		else if (settings.keysConfig.selectBall1.matches(event)) selectBall(1);
		else if (settings.keysConfig.selectBall2.matches(event)) selectBall(2);
		else if (settings.keysConfig.selectBall3.matches(event)) selectBall(3);
		else if (settings.keysConfig.selectBall4.matches(event)) selectBall(4);
	}

	const currentMaxScore = game.getCurrentMaxScore();

	return game.players.length ? (
		<div
			className="flex justify-around items-center content-center px-8 py-16 h-body"
			onKeyDown={handleKeyPress}
			tabIndex={-1}
		>
			<div className="flex flex-col justify-around p-8 bg-main-3 mr-8 rounded-lg h-full">
				<RoundButton text="Round 1" />
				<RoundButton text="Round 2" />
				<RoundButton text="Round 3" />
				<RoundButton text="New Round" />
			</div>
			<div className="w-full h-full rounded-lg p-8 bg-main-3">
				<div className="text-4xl text-center">
					Score to reach:{" "}
					<span className="underline text-4xl">{game.targetScore}</span>
				</div>
				<BallsComponent onClick={(i) => selectBall(i)} balls={game.balls} />
				<div className="flex justify-between mb-6">
					<div>
						<div className="flex flex-wrap">
							{game.players.map((player, i) => (
								<PlayerComponent
									key={i}
									game={game}
									player={player}
									isFirst={currentMaxScore === player.score}
								/>
							))}
						</div>
					</div>
					<div className="flex flex-col justify-between m-12 space-y-8">
						{game.useClaimPoints ? (
							<ActionButton
								text="Claim points"
								onClick={claimPoints}
								tooltip="Shortcut: [c]"
							/>
						) : null}
						<ActionButton
							text="Next Player"
							onClick={nextPlayer}
							tooltip="Shortcut: [n]"
						/>
						<div className="flex justify-around space-x-4">
							<UndoRedoButton
								flipH={false}
								text="Undo"
								onClick={undo}
								disabled={history.historyLength() < 2}
								tooltip="Cmd+Z"
							/>
							<UndoRedoButton
								flipH={true}
								text="Redo"
								onClick={redo}
								disabled={!history.canRedo()}
								tooltip="Cmd+Y"
							/>
						</div>
						<ActionButton
							onClick={() => {
								const comp = compress(history);
								saveGame(comp);
							}}
							text="Save Game"
							tooltip="Cmd+S"
						/>
					</div>
				</div>
			</div>
			<EndScreenPopUp
				isOpen={!hasClosed && game.gameEnded}
				onRequestClose={() => {
					setHasClosed(true);
				}}
				title={"Game ended"}
				winners={game.getWinners()}
			/>
		</div>
	) : (
		<Redirect to="/" />
	);
}
