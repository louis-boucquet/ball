import React from "react";
import { Ball } from "../../Logic/Types";
import BallComponent from "./BallComponent";

export default function BallsComponent({
	balls,
	onClick,
}: {
	balls: Ball[];
	onClick: (i: number) => void;
}) {
	return (
		<div className="flex justify-between w-full mt-12 mb-12">
			{balls.map((ball, i) => (
				<BallComponent
					key={i}
					onClick={() => onClick(i)}
					ball={ball}
					tooltip={i.toString()}
				/>
			))}
		</div>
	);
}
