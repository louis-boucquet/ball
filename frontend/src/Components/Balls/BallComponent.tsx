import React, { PropsWithChildren } from "react";
import { Ball, Size } from "../../Logic/Types";
import { BALLS } from "../../Logic/Constants";

interface BallProps {
	ball: Ball;
	onClick?: () => void;
	tooltip?: string;
	size?: Size;
}

const defaultBallProps: BallProps = {
	ball: BALLS[0],
	onClick: () => {},
	tooltip: "",
	size: Size.LARGE,
};

export default function BallComponent({
	ball,
	onClick,
	tooltip,
	size,
}: PropsWithChildren<BallProps>) {
	let sizeBall = "",
		sizeWhite = "",
		textSize = "";
	switch (size) {
		case Size.LARGE:
			sizeBall = "h-40 w-40";
			sizeWhite = "h-20 w-20";
			textSize = "text-6xl";
			break;
		case Size.MEDIUM:
			sizeBall = "h-8 w-8";
			sizeWhite = "h-4 w-4";
			textSize = "text-xs";
			break;
		case Size.SMALL:
			sizeBall = "h-5 w-5";
			sizeWhite = "h-2.5 w-2.5";
			textSize = "text-2xs leading-2";
			break;
	}
	// py-2 px-4"
	return (
		<div
			onClick={onClick}
			className={`grid place-items-center rounded-full ${sizeBall} cursor-pointer shadow bg-${
				ball.color
			} ${ball.active ? "" : "filter brightness-20"}`}
			title={`Shortcut: [${tooltip}]`}
		>
			<div
				className={`bg-main-4 ${sizeWhite} grid place-items-center rounded-full`}
			>
				<div className={`${textSize} text-black font-bold text-center`}>
					{ball.value}
				</div>
			</div>
		</div>
	);
}

BallComponent.defaultProps = defaultBallProps;
