module.exports = {
	purge: {
		content: ["./src/**/*.{js,jsx,ts,tsx}", "./public/index.html"],
		safelist: [
			'bg-red',
			'bg-yellow',
			'bg-white',
			'bg-blue',
			'bg-green',
			'bg-grey',
			'bg-black',
			'bg-transparent',
		]
	},
	darkMode: false, // or 'media' or 'class'
	theme: {
		extend: {
			brightness: {
				20: '.20',
			},
			height: {
				'0': '0',
				'header': '5%',
				'body': '95%',
			},
			fontSize:{
				'2xs': '0.5rem'
			},
			lineHeight:{
				'2': '0.5rem'
			}
		},
		colors: {
			main: {
				1: "#e0e0e0",
				2: "#bfbfbf",
				3: "#c5c5c5",
				4: "#ffffff",
			},
			red: "#ff0000",
			yellow: "#ffd700",
			white: "#fffafa",
			blue: "#000080",
			green: "#228B22",
			grey: {
				1: "#424242",
				2: "#979797"
			},
			black: "#000000",
			transparent: "transparent",
		},
		maxHeight: {
			'0': '0',
			'1/2': '50%',
			'9/10': '90%',
			'full': '100%',
		},
		maxWidth: {
			'0': '0',
			'1/2': '50%',
			'8/10': '80%',
			'9/10': '90%',
			'full': '100%',
		}
	},
	variants: {
	},
	plugins: [],
};
